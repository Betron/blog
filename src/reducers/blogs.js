import {DELETE_BLOG,GET_BLOGS} from '../actions/actionCreators'

const blogs = [];


export default (blogsState = blogs, action) => {

    const {type} = action

    switch (type){
        case DELETE_BLOG:
            return blogsState.filter((item, i) => i !== action.payload.index);
        case GET_BLOGS:
            return [...action.payload];
        default : break
    }
    return blogsState
}