import styled from 'styled-components'

export const MainTitle = styled.h1`
  text-align: center;
`;

export const ItemTitle = styled.div`
    font-size: 30px;
    font-weight: bold;
    text-align: center;
`;
export const ItemDescription = styled.div`
    margin 15px 0;
`;
export const ItemImage = styled.div`
    text-align: center;
    & img{
        max-width: 20%;
    }
`;
export const Blog = styled.div`
    display: flex;
    flex-direction: column;
    width: 740px;
    margin: 0 auto;
    font-family: 'Nunito', sans-serif;
`;
export const Comment = styled.div`
    margin-bottom: 20px;
    & span{
        font-weight: bold;
        margin-bottom: 10px;
    }
`;
export const Hello = styled.div`
    text-align: center;
`;