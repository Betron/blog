import blog from '../data/blogsList.json'

export const DELETE_BLOG = 'DELETE_BLOG'
export const GET_BLOGS = 'GET_BLOGS'

export function deleteBlog(index) {
    return{
        type: DELETE_BLOG,
        payload: {index},
    }
}

export const getBlogs = () => dispatch => {
    setTimeout(() =>{
        console.log('I get Blogs');
        dispatch({type: GET_BLOGS,payload: blog})
    },1000)
}
