import React, { Component } from 'react';
import BlogForm from './BlogForm'
import store from '../store'
import {Provider} from 'react-redux'
import {MainTitle} from "../styles"

class App extends Component {
  render() {
    return (
        <Provider store = {store}>
            <div>
                <MainTitle>Blog</MainTitle>
                <BlogForm/>
            </div>
        </Provider>
    );
  }
}

export default App;
