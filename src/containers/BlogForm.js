import React from 'react'
import {connect} from 'react-redux'
import Welcome from '../components/Welcome'
import {deleteBlog, getBlogs} from '../actions/actionCreators'
import {ItemTitle,Blog,ItemDescription,Comment,ItemImage,Hello} from "../styles/index";

class BlogForm extends React.Component{

    render(){
        return(
            <div>
                <Hello><Welcome name="Admin"/></Hello>
                {this.mapBlogs()}
            </div>
        )
    }
    componentDidMount() {
        this.props.getBlogs();
    }
    mapBlogs = () => {
        return (
            this.props.blogs.map((item, key) =>{
                return (
                    <Blog key={key}>
                        <div className="title">
                            <ItemTitle>{item.title}</ItemTitle>
                            <button onClick={() => this.props.deleteBlog(key)}>Delete this blog</button>
                            <ItemDescription>{item.description}</ItemDescription>
                            <ItemImage><img src={item.image} alt=""/></ItemImage>
                        </div>
                        {item.comments.map((comment, key) => {
                           return <Comment key={key}>
                               <div><span>Author: </span>{comment.author}</div>
                               <div><span>Text: </span>{comment.content}</div>
                           </Comment>
                        })}
                    </Blog>
                )
            })
        )
    }
}

export default connect((state) => ({
    blogs: state.blogsReducer
}),{deleteBlog, getBlogs})(BlogForm)
